## Angular samples
Project: Auto trade company shop

#### add-plant.component.ts
Component with logic of adding a new facility

#### anais-item.ts
Model with json2typescript

#### comment.service.ts
Service that allows to work with comments via API calls

#### current-infosite.service.ts
Service that works with staff data

#### infosite-view-detail.component.ts
Component that displays information of infosite

#### infosite.service.ts
Service that allows to work with infosite data via API calls

#### user-plant-list.component.ts
Lists of facilities connected to user

#### upload.directive.ts
Drag-and-Drop zone

#### user-manage.service.ts
Service that works with current users data

#### search-is.component.ts
Component that allows to do search with different creterias